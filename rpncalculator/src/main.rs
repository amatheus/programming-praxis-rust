use std::io;
use std::io::Write;
use rpncalculator::RpnCalculator;
use std::error::Error;

trait Evaluator {
    fn evaluate(&mut self, input: &str) -> Result<(), Box<dyn Error>>;
}

impl Evaluator for RpnCalculator {
    fn evaluate(&mut self, input: &str) -> Result<(), Box<dyn Error>> {
        for s in input.split(" ").map(str::trim).into_iter() {
            if let Ok(n) = s.trim().parse::<f64>() {
                self.push(n);
            } else {
                self.op(s)?;
            }
        }
        Ok(())
    }
}

enum ReplResult {
    Stop,
    Continue,
}

fn repl_step(calc: &mut RpnCalculator) -> Result<ReplResult, Box<dyn Error>> {
    print!("> ");
    io::stdout().flush().ok().expect("Could not flush stdout");
    let mut input = String::new();
    io::stdin().read_line(&mut input)
        .expect("Error reading line");
    if input == "q\n" {
        Ok(ReplResult::Stop)
    } else {
        calc.evaluate(&input)?;
        Ok(ReplResult::Continue)
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    let mut calc = RpnCalculator::default();
    println!("Calculator. Enter expressions, 'q' to quit.");
    loop {
        let res = repl_step(&mut calc)?;
        println!(":: {}", calc.top().unwrap_or(&0.0));
        if let ReplResult::Stop = res {
            break;
        }
    }
    Ok(())
}
