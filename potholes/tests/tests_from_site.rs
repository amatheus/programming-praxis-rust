use potholes::pothole_fixer;

#[test]
fn test_example1() {
    let input = ".X.";
    let result = pothole_fixer::fix(input);
    assert_eq!(result, 1);
}